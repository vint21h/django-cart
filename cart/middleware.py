# -*- coding: utf-8 -*-

from cart import Cart

__all__ = ['CartMiddleware', ]

class CartMiddleware(object):

    def process_request(self, request):

        request.cart = Cart(request)
