# -*- coding: utf-8 -*-

import datetime

from django.db.models.aggregates import Sum

import models

CART_ID = 'CART-ID'

class ItemAlreadyExists(Exception):
    pass

class ItemDoesNotExist(Exception):
    pass

class Cart:

    def __init__(self, request):

        self.cart_items = None
        cart_id = request.session.get(CART_ID)

        if cart_id:
            try:
                cart = models.Cart.objects.get(id=cart_id, checked_out=False)
            except models.Cart.DoesNotExist:
                cart = self.new(request)
        else:
            cart = self.new(request)
        self.cart = cart

    def __iter__(self):

        for item in self.items:
            yield item

    def new(self, request):

        cart = models.Cart(creation_date=datetime.datetime.now())
        cart.save()
        request.session[CART_ID] = cart.id
        return cart

    def add(self, product, unit_price, quantity=1):

        try:
            item = models.Item.objects.get(cart=self.cart, product=product)
        except models.Item.DoesNotExist:
            item = models.Item()
            item.cart = self.cart
            item.product = product
            item.unit_price = unit_price
            item.quantity = quantity
            item.save()
        else:
            raise ItemAlreadyExists

    def remove(self, product):

        try:
            item = models.Item.objects.get(cart=self.cart, product=product)
        except models.Item.DoesNotExist:
            raise ItemDoesNotExist
        else:
            item.delete()

    def update_quantity(self, product, quantity):

        try:
            item = models.Item.objects.get(cart=self.cart, product=product)
            item.quantity = quantity
            item.save()
        except models.Item.DoesNotExist:
            raise ItemDoesNotExist

    def clear(self):

        self.items.delete()

    def is_have_product(self, product):

        return self.items.filter(product=product).exist()

    @property
    def items_quantity(self):

        return self.items.count()

    @property
    def total_quantity(self):

        total_quantity = self.items.aggregate(Sum('quantity'))['quantity__sum']

        return int(0 if total_quantity is None else total_quantity) # small kludge if Sum return None

    @property
    def total_price(self):

        return sum([item.total_price for item in self.items])

    @property
    def items(self):

        if self.cart_items:

            return self.cart_items
        else:
            self.cart_items = self.cart.item_set.all()

            return self.cart_items

    @property
    def to_json(self):

        return {
            'items': [item.to_json for item in self.items],
            'total_price': self.total_price,
            'total_quantity': self.total_quantity,
            'items_quantity': self.items_quantity,
        }
