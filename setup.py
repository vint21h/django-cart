#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "django-cart",
    version = '.'.join(map(str, (0, 9, ))),
    packages = find_packages(),
    author = 'Marc Garcia',
    author_email = 'garcia.marc@gmail.com',
    description = 'A simple shopping cart for Django',
    license = 'GPLv3',
    url = 'https://code.google.com/p/django-cart/',
    zip_safe = False,
    include_package_data = True,
)
